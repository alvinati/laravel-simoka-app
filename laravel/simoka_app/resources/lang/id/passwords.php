<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Kata sandi Anda berhasil diganti',
    'sent' => 'Link untuk ganti kata sandi sudah dikirim melalui email Anda, silahkan cek email Anda.',
    'throttled' => 'Mohon tunggu sebentar sebelum mencoba lagi',
    'token' => 'Token kata sandi ini tida valid',
    'user' => "Pengguna dengan email tersebut tidak ditemukan.",

];
