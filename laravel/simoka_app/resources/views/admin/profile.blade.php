@extends('admin/main')

@section('style')
.i-profile {
    
}
@endsection

@section('adminContent')
<h1 class="h3 mb-3">User</h1>
<div class="col-12">
    <div class="card">
      <div class="card-body">
        <form id="form-profile" method="post">
        <div class="row">
            <div class="col-md-2">
            <img src="img/avatars/avatar-4.jpg" alt="Christina Mason" class="img-fluid rounded-circle mb-2" width="128" height="128">
            </div>
            <div class="col-md-3">
            <h3 class="align-middle"><strong>{{$profile->name}}</strong></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">E-mail</div>
            <div class="col-md-6 i-profile"><input id="email" name="email" class="form-control" value="{{$profile->email}}"/></div>
        </div>
        <div class="row">
            <div class="col-md-3">No HP</div>
            <div class="col-md-6 i-profile"><input id="mphone_number" class="form-control" value="{{$profile->mphone_number}}"/></div>
        </div>
        <div class="row">
            <div class="col-md-3">Kata Sandi</div>
            <div class="col-md-6 i-profile"><input id="password" class="form-control" value="******"/></div>
            <div class="col-md-3"><a class="btn btn-primary">Ubah Kata Sandi</a></div>
        </div>
        </form>
      </div>
    </div>
</div>
@endsection