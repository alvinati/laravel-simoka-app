@extends('admin/main')

@section('styles')
<style>
    th {
        font-size:14px;
    }
    td {
        font-size:14px;
    }

    img{
        width:50px;
        height:60px;
    }
</style>
@endsection

@section('modal')
<div class="modal fade" id="save-modal-dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Selesai</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body m-3">
      <div class="row">
        <p class="mb-0">Apakah Anda yakin untuk menyelesaikkan peminjaman?</p>
        <p style="font-size:12px;" class="mt-3">Peminjaman yang sudah diselesaikan tidak dapat diubah lagi</p>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
      <button type="button" onclick="saveConfirm('form-finish-book')" class="btn btn-danger m-save-btn">Yakin</button>
    </div>
  </div>
</div>
</div>
@endsection

@section('adminContent')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3 ><strong>
                Pengembalian
                </strong> Kendaraan</h3>
                <h5 class="card-subtitle text-muted mt-1">Harap masukkan semua data yang diperlukan</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-auto ms-auto text-end mt-n1">
                <h4><strong>Nomor. </strong>{{$data->book_number}}</h4>
            </div>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/peminjaman/{{$data->id}}/finish" enctype="multipart/form-data" id="form-finish-book">
            <table class="table table-md mt-2">
                <tbody>
                    <tr class="mb-2">
                        <th>Tanggal Pengajuan</th>
                        <td>: {{ $data->request->created_at->locale('id')->isoFormat('dddd, D MMMM Y') }}</td>
                        <th>Tujuan</th>
                        <td>: {{ $data->request->destination }}</td>
                    </tr>
                    <tr>
                        <th>Nama Pegawai</th>
                        <td>: {{ $data->requestor }}</td>
                        <th>Tanggal Berangkat</th>
                        <td>: {{ $data->request->travel_date }}</td>
                    </tr>
                    <tr>
                        <th>Keperluan</th>
                        <td>: {{ $data->requisite }}</td>
                        <th>Jam Berangkat</th>
                        <td>: {{ $data->request->travel_time }}</td>
                    </tr>
                    <tr>
                        <th>Pengemudi</th>
                        <td>: {{$data->driver_name}}</td>
                        <th>Mobil</th>
                        <td>: {{$data->car}}</td>
                    </tr>
                    @csrf
                    <input type="hidden" id="id" name="id" value="{{$data->id}}">
                    <tr>
                        @if($data->status->id==1)
                        <th>Biaya Bensin</th>
                        <td><input  class="form-control" min="0" type="number" id="fuel" name="fuel" onchange="updateTotal()"></input></td>
                        <th>Biaya Tol</th>
                        <td><input class="form-control" min="0" type="number" id="toll" name="toll" onchange="updateTotal()"></input></td>
                        @else
                        <th>Biaya Bensin</th>
                        <td>: {{ $data->fuel_cost_string}}</td>
                        <th>Biaya Tol</th>
                        <td>: {{$data->toll_cost_string}}</td>
                        @endif
                    </tr>

                    @if($data->status->id==1)
                    <tr>
                        <th>Upload Struk bensin
                            <label class="text-muted" style="font-size:10px;">*maksimal 2 MB/foto</label>
                        </th>
                        <td>
                            <input type="file" class="form-control" id="fuel_receipts" name="fuel_receipts[]" onchange="readURL(this, '#fuel')" multiple hidden/>
                            <label class="btn btn-primary btn-fuel" for="fuel_receipts">Upload</label>
                        </td>
                        <th>Upload Struk toll
                            <label class="text-muted"style="font-size:10px;">*maksimal 2 MB/foto</label>
                        </th>
                        <td>
                            <input type="file" class="form-control" id="toll_receipts" name="toll_receipts[]" onchange="readURL(this, '#toll')" multiple hidden/>
                            <label class="btn btn-primary" for="toll_receipts">Upload</label>
                        </td>
                    </tr>

                    <tr>
                    <th>
                        @for($i=1; $i<=3 ; $i++)
                        <img class="mb-1 me-1" id="fuel-{{$i}}" src="{{url('storage/'.$p->image_path)}}" alt="" title=""/>
                        @endfor                         
                    </th>
                    <td>
                    </td>

                    <th>
                        @for($i=1; $i<=3 ; $i++)
                        <img class="mb-1 me-1" id="toll-{{$i}}" src="{{url('storage/'.$p->image_path)}}" alt="" title=""/>
                        @endfor                            
                    </th>
                    <td>
                    </td>
                    </tr>
                    @else
                        <tr>
                            <th>Struk Pembelian Bensin</th>
                            <td>
                            </td>

                            @if(isset($data->toll_cost))
                            <th>Struk Pembayaran Toll</th>
                            <td>
                            </td>
                            @endif
                        </tr>
                        <tr>
                            <th>
                            @if(isset($data->fuel_receipt_paths))
                                @foreach($data->fuel_receipt_paths as $p)
                                    <img class="mb-1 me-1" src="{{ url('/storage'.$p->image_path)}}"/>
                                @endforeach
                            @endif
                            </th>
                            <td></td>

                            @if(isset($data->toll_cost) && isset($data->toll_receipt_paths))
                            <th>@foreach($data->toll_receipt_paths as $p) 
                                    <img class="mb-1 me-1" src="{{ url('storage/'.$p->image_path) }}"/>
                                @endforeach</th>
                            <td>
                            </td>
                            @endif
                        </tr>
                    @endif
                </tbody>
            </table>

            <div class="row justify-content-md-center">
                <div class="col-md-2"><strong>Total Biaya</strong></div>

                @if($data->status->id == 1)
                <div class="col-md-3">
                    <input class="form-control" type="number" id="total" name="total" hidden/>
                    <div id="total-cost">: Rp. 0,00</div>
                </div>
                @else 
                <div class="col-md-3">
                    <span>: {{ $data->total_cost_string }}</span>
                </div>
                @endif
            </div>

            @if($data->status->id == 1)
            <div class="d-flex flex-row-reverse mt-5">
                <a href="/peminjaman" class="btn btn-secondary">Batal </a>
                <a data-bs-toggle="modal" data-bs-target="#save-modal-dialog" class="btn btn-primary me-3">Peminjaman Selesai </a>
            </div>
            @endif
        </form>
      </div>
    </div>
  </div>
</div>

<script>

function saveConfirm(formId) {
    document.getElementById(formId).submit();   
}

function readURL(input, imageId) {

  if (input.files && input.files.length > 0) {
    for(var i=0; i < 3; i++) {
        var reader = new FileReader();
        var idNum = i+1;
        var id = imageId+"-" + idNum;

        reader.onload = assignImageSource(id);

        if(input.files[i]){
            reader.readAsDataURL(input.files[i]); // convert to base64 string
        } else {
            $(id).attr('src', "{{ url('/storage/place_holder.png') }}");
        }
    }
  }
}

function assignImageSource(id) {
    return function(e) {
        $(id).attr('src', e.target.result);
    }
}

function updateTotal() {
    var fuel = document.getElementById("fuel").value;
    var toll = document.getElementById("toll").value;

    if(fuel == null || fuel == ""){
        fuel = "0";
    }

    if(toll == null || toll == "") {
        toll = "0";
    }

    var cost = parseFloat(fuel) + parseFloat(toll);

    var formatter = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        // These options are needed to round to whole numbers if that's what you want.
        minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        maximumFractionDigits: 2, // (causes 2500.99 to be printed as $2,501)
    });

    var formatted = formatter.format(cost);
    document.getElementById("total").value = cost;
    document.getElementById("total-cost").innerHTML = formatted;
}

</script>
@endsection
