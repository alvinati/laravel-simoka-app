@extends('admin/main')

@section('modal')
<div class="modal fade" id="driverFormModal" tabindex="-1" aria-labelledby="driverFormLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="driverFormLabel">Ubah Data Pengemudi</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form class="container-fluid" action="" method="post" id="driverForm">
          @csrf
          <input type="hidden" id="id" name="id">
          <div class="row mb-3">
            <div class="col-3">
              <label for="name" class="col-form-label">Nama</label>
            </div>
            <div class="col-9">
              <input type="text" class="form-control" id="name" name="name">
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-3">
              <label for="waNumber" class="col-form-label">No Whatsapp</label>
            </div>
            <div class="col-9">
              <input class="form-control" type="tel" id="waNumber" name="waNumber"></input>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-3">
              <label for="address" class="col-form-label">Alamat</label>
            </div>
            <div class="col-9">
              <input class="form-control" type="text" id="address" name="address"></input>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-3">
              <label for="car_id" class="col-form-label">Mobil Dinas</label>
            </div>
            <div class="col-4">
              <select class="form-select" aria-label="Pilih Mobil Dinas" id="car_id" name="car_id">
                <option value="0" selected>Pilih Mobil</option>
                  @foreach($cars as $c)
                  <option value="{{ $c->id }}">{{ $c->plat_number }}</option>
                  @endforeach
              </select>
            </div>
          </div>
          @if(count($cars) < 1)
          <div class="row mb-3">
            <p style="font-size:12px;">Semua kendaraan sudah di assign / data kendaraan masih kosong<a class="badge bg-success text-white ms-2 btn-to-vehicle" href="" target="_blank">Tambah kendaraan<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-external-link align-middle me-2"><path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path><polyline points="15 3 21 3 21 9"></polyline><line x1="10" y1="14" x2="21" y2="3"></line></svg></a></p>
          </div>
          @endif
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-employee-form">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('adminContent')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div id="datatables-reponsive_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
          <div class="row">
            <div class="col-sm-6 col-md-3 mb-3">
              <div class="row"> 
                  <form method="get" action="{{ route('pengemudi.search') }}">
                    @csrf
                    <div class="input-group">
                     @if($searchKey != "")
                      <input type="text" name="searchkey" class="form-control form-control-sm" value="{{ $searchKey }}" placeholder="Pencarian Pengemudi" aria-label="Search">
                      <a class="btn btn-danger py-2" href="/pegawai"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle me-2">
                        <circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15">
                        </line></svg>
                      </a>
                      @else
                      <input type="text" name="searchkey" class="form-control form-control-sm" placeholder="Pencarian kendaraan" aria-label="Search">
                      <button class="btn btn-primary" type="submit">Cari</button>
                      @endif
                    </div>
                  </form>
              </div>
            </div>
          <div class="row">
            <div class="col-sm-12">
              <table id="datatables-reponsive" class="table table-striped dataTable no-footer dtr-inline collapsed" style="width: 100%;" role="grid" aria-describedby="datatables-reponsive_info">
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-sort="ascending" aria-label="Nama: activate to sort column descending">
                      Nama
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="No HP: activate to sort column ascending">
                      No Whatsapp
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Alamat: activate to sort column ascending">
                      Alamat
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Mobil Dinas: activate to sort column ascending">
                      Mobil Dinas
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Kehadiran: activate to sort column ascending">
                      Kehadiran
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Aksi: activate to sort column ascending">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>	
                  @foreach($drivers as $d)
                    @if($loop->iteration % 2 == 0)									
                  <tr class="odd">
                    <td class="" tabindex="0">{{ $d->employee->name}}</td>
                    <td style="">{{ $d->employee->wa_number }}</td>
                    <td style="">{{ $d->employee->address }}</td>
                    @if($d->car != NULL)
                    <td style="">{{ $d->car->plat_number }}</td>
                    @else
                    <td style=""></td>
                    @endif
                    <td style="">{{ $d->status->status }}</td>
                    <td class="table-action">
                      @if($d->soft_deleted == 0)
                      <a class="edit-icon" href="#" data-bs-toggle="modal" data-bs-target="#driverFormModal" 
                         data-drv='{ "id": "{{ $d->id }}", 
                                      "name": "{{ $d->employee->name }}",
                                      "waNumber": "{{ $d->employee->mphone_number }}",
                                      "address": "{{ $d->employee->address}}",
                                      <?php 
                                      if($d->car != NULL || $d->car_id != 0)
                                      echo '"plat": "'. $d->car->plat_number .'",';
                                      ?>
                                      "status": "{{ $d->status->id }}"
                                    }'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                          <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                        </svg>
                      </a>
                      @else
                      <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                      @endif
				          	</td>
                  </tr>
                  @else
                  <tr class="even">
                    <td class="" tabindex="0">{{ $d->employee->name}}</td>
                    <td style="">{{ $d->employee->wa_number }}</td>
                    <td style="">{{ $d->employee->address }}</td>
                    @if($d->car != NULL)
                    <td style="">{{ $d->car->plat_number }}</td>
                    @else
                    <td style=""></td>
                    @endif
                    <td style="">{{ $d->status->status }}</td>
                    <td class="table-action">
                      @if($d->soft_deleted == 0)
                      <a class="edit-icon" href="#" data-bs-toggle="modal" data-bs-target="#driverFormModal" 
                         data-drv='{ "id": "{{ $d->id }}", 
                                      "name": "{{ $d->employee->name }}",
                                      "waNumber": "{{ $d->employee->mphone_number }}",
                                      "address": "{{ $d->employee->address}}",
                                      <?php 
                                      if($d->car != NULL || $d->car_id != 0)
                                      echo '"plat": "'. $d->car->plat_number .'",';
                                      ?>
                                      "status": "{{ $d->status->id }}"
                                    }'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                          <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                        </svg>
                      </a>
                      @else
                      <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                      @endif
					          </td>
                  </tr>
                    @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-5">
                <div class="dataTables_info" id="datatables-reponsive_info" role="status" aria-live="polite">
                 
                </div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers" id="datatables-reponsive_paginate">
                <ul class="pagination mt-3">

                  {{ $drivers->links() }}
              
                  <!-- <li class="paginate_button page-item previous disabled" id="datatables-reponsive_previous">
                    <a href="{{ $drivers->previousPageUrl() }}" aria-controls="datatables-reponsive" data-dt-idx="0" tabindex="0" class="page-link">
                      Sebelumnya</a>
                  </li>
                  <li class="paginate_button page-item next" id="datatables-reponsive_next">
                    <a href="{{ $drivers->nextPageUrl() }}" aria-controls="datatables-reponsive" data-dt-idx="7" tabindex="0" class="page-link">
                      Selanjutnya</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function() {

  $(".btn-to-vehicle").click(function() {
    var url = "{{route('kendaraan')}}"
    window.location.href = url;
  });

  $(".edit-icon").click(function() {
    var drv = $(this).data('drv');

     $('#driverFormModal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val(drv.id);
        modal.find('#name').val(drv.name);
        modal.find('#waNumber').val(drv.waNumber);
        modal.find('#address').val(drv.address);

        if(drv.plat != null ) {
          document.getElementsByName('car_id')[0].options[0].innerHTML = drv.plat;
        } else {
          document.getElementsByName('car_id')[0].options[0].innerHTML = "Pilih Mobil";
        }

        modal.find('#driverForm').attr("action", "{{ route('pengemudi.update') }}");
     });
  });
});

</script>
@endsection
