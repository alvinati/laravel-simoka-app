@extends('admin/main')

@section('modal')

<div class="modal fade" id="vehicleFromModal" tabindex="-1" aria-labelledby="vehicleFormLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="vehicleFormLabel">Tambah Kendaraan</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form class="container-fluid" action="" method="post" id="vechicleForm">
          @csrf
          <input type="hidden" id="id" name="id">
          <div class="row mb-3">
            <div class="col-3">
              <label for="plat_number" class="col-form-label">Plat Nomor </label>
            </div>
            <div class="col-9">
              <input type="text" class="form-control" id="plat_number" name="plat_number">
            </div>
          </div>
          <div class="row mb-3">
           <div class="col-3">
            <label for="brand" class="col-form-label">Merek Mobil</label>
            </div>
            <div class="col-9">
            <input class="form-control" id="brand" name="brand"></input>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-3">
              <label for="model_version" class="col-form-label">Model Mobil</label>
            </div>
            <div class="col-9">
              <input class="form-control" type="text" id="model_version" name="model_version"></input>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-3">
             <label for="km" class="col-form-label">Jarak Ditempuh (KM)</label>
            </div>
            <div class="col-9">
              <input class="form-control" type="number" id="km" name="km"></input>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-employee-form">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteRow" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Hapus Data</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body m-3">
      <p class="mb-0">Apakah Anda yakin untuk menghapus data?</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
      <button type="button" class="btn btn-danger m-delete-btn">Hapus</button>
    </div>
  </div>
</div>
</div>
@endsection

@section('adminContent')
  <div class="col-12">
    <div class="card">
      <div class="card-header">
          <a href="#" class="btn btn-primary float-end mt-n1 btn-add-new" data-bs-toggle="modal" data-bs-target="#vehicleFromModal">+ Tambah Kendaraan</a>
      </div>
      <div class="card-body">
        <div id="datatables-reponsive_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
          <div class="row">
            <div class="col-sm-6 col-md-3 mb-3">
              <div class="row"> 
                <form method="get" action="{{ route('kendaraan.search') }}">
                  @csrf
                  <div class="input-group">
                    @if($searchKey != "")
                    <input type="text" name="searchkey" class="form-control form-control-sm" value="{{ $searchKey }}" placeholder="Pencarian kendaraan" aria-label="Search">
                    <a class="btn btn-danger py-2" href="/kendaraan"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle me-2">
                      <circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15">
                      </line></svg>
                    </a>
                    @else
                    <input type="text" name="searchkey" class="form-control form-control-sm" placeholder="Pencarian kendaraan" aria-label="Search">
                    <button class="btn btn-primary" type="submit">Cari</button>
                    @endif
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <table id="datatables-reponsive" class="table table-striped dataTable no-footer dtr-inline collapsed" style="width: 100%;" role="grid" aria-describedby="datatables-reponsive_info">
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-sort="ascending" aria-label="Plat Nomor: activate to sort column descending">
                      Plat Nomor
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Merek Mobil: activate to sort column ascending">
                      Merek Mobil
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Model Mobil: activate to sort column ascending">
                      Model Mobil
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Jarak Ditempuh: activate to sort column ascending">
                      Jarak Ditempuh (KM)
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Status: activate to sort column ascending">
                      Status
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Aksi: activate to sort column ascending">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>	
                  @foreach($vehicles as $v)
                    @if($loop->iteration % 2 == 0)									
                  <tr class="odd">
                    <td class="" tabindex="0">{{ $v->plat_number}}</td>
                    <td style="">{{ $v->brand }}</td>
                    <td style="">{{ $v->model_version }}</td>
                    <td style="">{{ $v->km }}</td>
                    @if($v->status->id == 1)
                    <td style=""><span class="badge bg-success">{{ $v->status->status }}</span></td>
                    @elseif($v->status->id == 2 )
                    <td style=""><span class="badge bg-danger">{{ $v->status->status }}</span></td>
                    @else
                    <td style=""><span class="badge">{{ $v->status->status }}</span></td>
                    @endif
                    <td class="table-action">
                    @if($v->soft_deleted == 0)
                      <a class="edit-icon" href="#" data-bs-toggle="modal" data-bs-target="#vehicleFromModal" 
                         data-car='{ "id": "{{ $v->id }}", 
                                      "plat": "{{ $v->plat_number }}",
                                      "brand": "{{ $v->brand }}",
                                      "version": "{{ $v->model_version}}",
                                      "status": "{{ $v->status->id }}",
                                      "km": "{{ $v->km }}"
                                    }'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                          <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                        </svg>
                      </a>
                      <a class="delete-icon" href="#" data-bs-toggle="modal" data-bs-target="#deleteRow" data-id="{{ $v->id }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                          <polyline points="3 6 5 6 21 6"></polyline>
                          <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                          </path>
                        </svg>
                      </a>
                      @else
                      <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                      @endif
				          	</td>
                  </tr>
                    @else
                  <tr class="even">
                    <td class="" tabindex="0">{{ $v->plat_number}}</td>
                    <td style="">{{ $v->brand }}</td>
                    <td style="">{{ $v->model_version }}</td>
                    <td style="">{{ $v->km }}</td>
                    @if($v->status->id == 1)
                    <td style=""><span class="badge bg-success">{{ $v->status->status }}</span></td>
                    @elseif($v->status->id == 2 )
                    <td style=""><span class="badge bg-danger">{{ $v->status->status }}</span></td>
                    @else
                    <td style=""><span class="badge">{{ $v->status->status }}</span></td>
                    @endif
                    <td class="table-action">
                    @if($v->soft_deleted == 0)
                      <a class="edit-icon" href="#" data-bs-toggle="modal" data-bs-target="#vehicleFromModal" 
                         data-car='{ "id": "{{ $v->id }}", 
                                      "plat": "{{ $v->plat_number }}",
                                      "brand": "{{ $v->brand }}",
                                      "version": "{{ $v->model_version}}",
                                      "status": "{{ $v->status->id }}",
                                      "km": "{{ $v->km }}"
                                    }'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                          <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                        </svg>
                      </a>
                      <a class="delete-icon" href="#" data-bs-toggle="modal" data-bs-target="#deleteRow" data-id="{{ $v->id }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                          <polyline points="3 6 5 6 21 6"></polyline>
                          <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                          </path>
                        </svg>
                      </a>
                      @else
                      <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                      @endif
					          </td>
                  </tr>
                      @endif
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-5">
                <div class="dataTables_info" id="datatables-reponsive_info" role="status" aria-live="polite">
                 
                </div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers" id="datatables-reponsive_paginate">
                <ul class="pagination mt-3">

                  {{ $vehicles->links() }}
              
                  <!-- <li class="paginate_button page-item previous disabled" id="datatables-reponsive_previous">
                    <a href="{{ $vehicles->previousPageUrl() }}" aria-controls="datatables-reponsive" data-dt-idx="0" tabindex="0" class="page-link">
                      Sebelumnya</a>
                  </li>
                  <li class="paginate_button page-item next" id="datatables-reponsive_next">
                    <a href="{{ $vehicles->nextPageUrl() }}" aria-controls="datatables-reponsive" data-dt-idx="7" tabindex="0" class="page-link">
                      Selanjutnya</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<script>
$(document).ready(function() {
  $(".delete-icon").click(function() {
    var id = $(this).data('id');
    $(".m-delete-btn").click(function() {
      var url = "{{ route('kendaraan.delete', ':id') }}";
      url = url.replace(':id', id);
      window.location.href = url;
    });
  });


  $(".btn-add-new").click(function() {

    document.getElementById('vehicleFormLabel').textContent = 'Tambah Kendaraan';

    $('#vehicleFromModal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val("");
        modal.find('#plat_number').val("");
        modal.find('#brand').val("");
        modal.find('#model_version').val("");
        modal.find('#km').val("");
        modal.find('#vechicleForm').attr("action", "{{ route('kendaraan.save') }}");
    });
  });


  $(".edit-icon").click(function() {
    var car = $(this).data('car');

    document.getElementById('vehicleFormLabel').textContent = 'Ubah Kendaraan';

     $('#vehicleFromModal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val(car.id);
        modal.find('#plat_number').val(car.plat);
        modal.find('#brand').val(car.brand);
        modal.find('#model_version').val(car.version);
        modal.find('#km').val(car.km);

        modal.find('#vechicleForm').attr("action", "{{ route('kendaraan.update') }}");
     });
  });
});

</script>
@endsection
