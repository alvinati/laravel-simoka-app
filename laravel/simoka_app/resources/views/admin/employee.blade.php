@extends('admin/main')

@section('modal')

<div class="modal fade" id="employeeFormModal" tabindex="-1" aria-labelledby="employeeDataLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="employeeDataLabel">Tambah Pegawai</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form class="container-fluid" action="" method="post" id="employeeForm">
          @csrf
          <input type="hidden" id="id" name="id">
          <div class="row mb-3">
            <div class="col-2">
              <label for="name" class="col-form-label">Nama </label>
            </div>
            <div class="col-10">
              <input type="text" class="form-control" id="name" name="name">
            </div>
          </div>
          <div class="row mb-3">
           <div class="col-2">
            <label for="nik" class="col-form-label">NIK</label>
            </div>
            <div class="col-10">
            <input class="form-control" id="nik" name="nik"></input>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-2">
              <label for="role" class="col-form-label">Role User</label>
            </div>
            <div class="col-5">
              <select class="form-select" aria-label="Pilih Peran Pengguna" id="role" name="role">
                <option value="0" selected>Pilih Peran Pengguna</option>
                @foreach($roles as $r)
                <option value="{{ $r->id }}">{{ $r->role }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-2">
              <label for="division" class="col-form-label">Divisi</label>
            </div>
            <div class="col-4">
              <select class="form-select" aria-label="Pilih Divisi Pegawai" id="division" name="division">
                <option value="0" selected>Pilih Divisi</option>
                @foreach($divisions as $d)
                <option value="{{ $d->id }}">{{ $d->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-2">
              <label for="title" class="col-form-label">Jabatan</label>
            </div>
            <div class="col-4">
              <select class="form-select" aria-label="Pilih Jabatan Pegawai" id="title" name="title">
                <option value="0" selected>Pilih Jabatan</option>
                @foreach($titles as $t)
                <option value="{{ $t->id }}">{{ $t->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-2">
              <label for="phoneNumber" class="col-form-label">No HP</label>
            </div>
            <div class="col-10">
              <input class="form-control" type="tel" id="phoneNumber" name="phoneNumber"></input>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-2">
             <label for="waNumber" class="col-form-label">No WA</label>
            </div>
            <div class="col-10">
              <input class="form-control" type="tel" id="waNumber" name="waNumber"></input>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-2">
              <label for="address" class="col-form-label">Alamat</label>
            </div>
            <div class="col-10">
              <textarea class="form-control" id="address" name="address"></textarea>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-2">
             <label for="email">Email</label>
            </div>
            <div class="col-10">
              <input class="input-email form-control form-control-sm" id="email" name="email"></input>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-employee-form">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteRow" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Hapus Data</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body m-3">
      <div class="row">
        <p class="mb-0">Apakah Anda yakin untuk menghapus data?</p>
        <p style="font-size:12px;" class="mt-3">Catatan: </p>
        <ul class="ms-3">
          <li style="font-size:12px;">Pegawai yang terkait dengan data peminjaman atau dengan jabatan pengemudi hanya bisa dinonaktifkan</li>
        </ul>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
      <button type="button" class="btn btn-danger m-delete-btn">Hapus</button>
    </div>
  </div>
</div>
</div>
@endsection

@section('adminContent')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
          <a href="#" class="btn btn-primary float-end mt-n1 btn-add-new" data-bs-toggle="modal" data-bs-target="#employeeFormModal">+ Tambah Pegawai</a>
      </div>
      <div class="card-body">
        <div id="datatables-reponsive_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
          <div class="row">
            <div class="col-sm-6 col-md-3 mb-3">
              <div class="row"> 
                  <form method="get" action="{{ route('pegawai.search') }}">
                    @csrf
                    <div class="input-group">
                     @if($searchKey != "")
                      <input type="text" name="searchkey" class="form-control form-control-sm" value="{{ $searchKey }}" placeholder="Pencarian kendaraan" aria-label="Search">
                      <a class="btn btn-danger py-2" href="/pegawai"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle align-middle me-2">
                        <circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15">
                        </line></svg>
                      </a>
                      @else
                      <input type="text" name="searchkey" class="form-control form-control-sm" placeholder="Pencarian kendaraan" aria-label="Search">
                      <button class="btn btn-primary" type="submit">Cari</button>
                      @endif
                    </div>
                  </form>
              </div>
            </div>
          <div class="row">
            <div class="col-sm-12">
              <table id="datatables-reponsive" class="table table-striped dataTable no-footer dtr-inline collapsed" style="width: 100%;" role="grid" aria-describedby="datatables-reponsive_info">
                <thead>
                  <tr>
                    <th class="sorting_asc" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-sort="ascending" aria-label="Nama: activate to sort column descending">
                      Nama
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="NIK: activate to sort column ascending">
                      NIK
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Divisi: activate to sort column ascending">
                      Divisi
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Jabatan: activate to sort column ascending">
                      Jabatan
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="No HP: activate to sort column ascending">
                      No HP
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Alamat: activate to sort column ascending">
                      Alamat
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Email: activate to sort column ascending">
                      Email
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Aksi: activate to sort column ascending">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>	
                  @foreach($employees as $e)
                    @if($loop->iteration % 2 == 0)									
                  <tr class="odd">
                    <td class="" tabindex="0">{{ $e->name}}</td>
                    <td style="">{{ $e->NIK }}</td>
                    <td style="">{{ $e->division->name }}</td>
                    <td style="">{{ $e->title->name }}</td>
                    <td style="">{{ $e->mphone_number }}</td>
                    <td style="">{{ $e->address }}</td>
                    <td style="">{{ $e->email }}</td>
                    <td class="table-action">
                    @if($e->soft_deleted == 0)
                      <a class="edit-icon" href="#" data-bs-toggle="modal" data-bs-target="#employeeFormModal" 
                         data-empl='{ "id": "{{ $e->id }}", 
                                      "name": "{{ $e->name }}",
                                      "nik": "{{ $e->NIK }}",
                                      "division": "{{ $e->division->id}}",
                                      "title": "{{ $e->title->id }}",
                                      "role" : "{{$e->user->role}}",
                                      "phoneNumber": "{{ $e->mphone_number }}",
                                      "waNumber": "{{ $e->wa_number }}",
                                      "address": "{{ $e->address }}",
                                      "email": "{{ $e->email }}"
                                    }'>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                          <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                        </svg>
                      </a>
                      <a class="delete-icon" href="#" data-bs-toggle="modal" data-bs-target="#deleteRow" data-id="{{ $e->id }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                          <polyline points="3 6 5 6 21 6"></polyline>
                          <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                          </path>
                        </svg>
                      </a>
                      @else
                      <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                      @endif
										</td>
                  </tr>
                    @else
                  <tr class="even">
                    <td class="" tabindex="0">{{ $e->name}}</td>
                      <td style="">{{ $e->NIK }}</td>
                      <td style="">{{ $e->division->name }}</td>
                      <td style="">{{ $e->title->name }}</td>
                      <td style="">{{ $e->mphone_number }}</td>
                      <td style="">{{ $e->address }}</td>
                      <td style="">{{ $e->email }}</td>
                      <td class="table-action">
                      @if($e->soft_deleted == 0)
                        <a class="edit-icon" href="#" data-bs-toggle="modal" data-bs-target="#employeeFormModal" 
                        data-empl='{ "id": "{{ $e->id }}", 
                                      "name": "{{ $e->name }}",
                                      "nik": "{{ $e->NIK }}",
                                      "division": "{{ $e->division->id }}",
                                      "title": "{{ $e->title->id }}",
                                      "role" : "{{$e->user->role}}",
                                      "phoneNumber": "{{ $e->mphone_number }}",
                                      "waNumber": "{{ $e->wa_number }}",
                                      "address": "{{ $e->address }}",
                                      "email": "{{ $e->email }}"
                                    }'>
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                          </svg>
                        </a>
                        <a class="delete-icon" href="#" data-bs-toggle="modal" data-bs-target="#deleteRow" data-id="{{ $e->id }}">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                            <polyline points="3 6 5 6 21 6"></polyline>
                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                          </svg>       
                        </a>
                        @else 
                          <a href="#" class="btn btn-secondary btn-sm disabled">Tidak Aktif</a>
                        @endif
										  </td>
                    </tr>
                      @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-5">
                <div class="dataTables_info" id="datatables-reponsive_info" role="status" aria-live="polite">
                 
                </div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers" id="datatables-reponsive_paginate">
                <ul class="pagination mt-3">

                  {{ $employees->links() }}
              
                  <!-- <li class="paginate_button page-item previous disabled" id="datatables-reponsive_previous">
                    <a href="{{ $employees->previousPageUrl() }}" aria-controls="datatables-reponsive" data-dt-idx="0" tabindex="0" class="page-link">
                      Sebelumnya</a>
                  </li>
                  <li class="paginate_button page-item next" id="datatables-reponsive_next">
                    <a href="{{ $employees->nextPageUrl() }}" aria-controls="datatables-reponsive" data-dt-idx="7" tabindex="0" class="page-link">
                      Selanjutnya</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function() {
  $(".delete-icon").click(function() {
    var id = $(this).data('id');
    $(".m-delete-btn").click(function() {
      var url = "{{ route('pegawai.delete', ':id') }}";
      url = url.replace(':id', id);
      window.location.href = url;
    });
  });


  $(".btn-add-new").click(function() {

    document.getElementById('employeeDataLabel').textContent = 'Tambah Pegawai';

    $('#employeeFormModal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val("");
        modal.find('#name').val("");
        modal.find('#nik').val("");
        modal.find('#division').val(0);
        modal.find('#title').val(0);
        modal.find('#role').val(0);
        modal.find('#phoneNumber').val("");
        modal.find('#waNumber').val("");
        modal.find('#address').val("");
        modal.find('#email').val("");
        modal.find('#employeeForm').attr("action", "{{ route('pegawai.save') }}");
    });
  });


  $(".edit-icon").click(function() {
    var empl = $(this).data('empl');

    document.getElementById('employeeDataLabel').textContent = 'Ubah Pegawai';

     $('#employeeFormModal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#id').val(empl.id);
        modal.find('#name').val(empl.name);
        modal.find('#nik').val(empl.nik);
        modal.find('#division').val(empl.division);
        modal.find('#title').val(empl.title);
        modal.find('#role').val(empl.role);
        modal.find('#phoneNumber').val(empl.phoneNumber);
        modal.find('#waNumber').val(empl.waNumber);
        modal.find('#address').val(empl.address);
        modal.find('#email').val(empl.email);

        modal.find('#employeeForm').attr("action", "{{ route('pegawai.update') }}");
     });
  });
});

</script>
@endsection
