@extends('admin/main')

@section('adminContent')
<div class="row mb-2 mb-xl-3">
  <div class="col-auto d-none d-sm-block mb-3">
    <h3><strong>Detail</strong></h3>
  </div>
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div id="datatables-reponsive_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
          <div class="row">
            <div class="col-sm-12">
              <table id="datatables-reponsive" class="table table-striped dataTable no-footer dtr-inline collapsed" style="width: 100%;" role="grid" aria-describedby="datatables-reponsive_info">
                <thead>
                  <tr>
                    <th>Nama Pegawai</th>
                    <th>{{$data->employee->name}}</th>
                  </tr>
                </thead>
                <tbody>	
                    <tr>
                        <th>Divisi</th>
                        <td>{{$data->employee->division->name}}</td>
                    </tr>
                    <tr>
                        <th>Jabatan</th>
                        <td>{{$data->employee->title->name}}</td>
                    </tr>
                    <tr>
                        <th>Driver</th>
                        <td>{{$data->driver}}</td>
                    </tr>
                    <tr>
                        <th>Mobil</th>
                        <td>{{$data->car}}</td>
                    </tr>
                    <tr>
                        <th>Keperluan</th>
                        <td>{{$data->request->requisite}}</td>
                    </tr>
                    <tr>
                        <th>Tujuan</th>
                        <td>{{$data->request->destination}}</td>
                    </tr>
                    <tr>
                        <th>Tanggal Berangkat</th>
                        <td>{{$data->exit_date}}</td>
                    </tr>
                    <tr>
                        <th>Tanggal Pulang</th>
                        <td>{{$data->return_date}}</td>
                    </tr>
                    <tr>
                        <th>Biaya Bensin</th>
                        <td>{{$data->fuel_cost}}</td>
                    </tr>
                    <tr>
                        <th>Biaya Tol</th>
                        <td>{{$data->toll_cost}}</td>
                    </tr>
                    <tr>
                        <th>Total Biaya</th>
                        <td>{{$data->total_cost}}</td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-end">
  <div class="col-md-1">
  <a class="btn btn-primary" href="/laporan">Kembali</a>
  </div>
</div>
</div>

@endsection
