@extends('admin/main')

@section('modal')
<div class="modal fade" id="divisionFormModal" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
<form method="post" id="divisionForm" action="/divisi-jabatan/simpan">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Ubah Divisi</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body m-3">
        <div class="row">
            <div class="col-md-2">No</div>
            <div class="col-md-8">Nama</div>
        </div>
        @csrf
        <input name="change_key" value="division" class="form-control" hidden/>
        @foreach($division as $key=>$d) 
        <div class="row mt-2">
            <div class="col-md-2">{{$key+1}}<input class="form-control" name="ids[]" value="{{$d->id}}" hidden/></div>
            <div class="col-md-8"><input class="form-control" name="divisions[]" value="{{$d->name}}"/></div>
        </div>
        @endforeach
        <div id="new-row"></div>
        
      <div class="row justify-content-md-center">
        <button type="button" id="add-division" class="btn btn-outline-primary col-11 mt-4">Tambah Divisi</button>    
      </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="save-btn">Simpan</button>
    </div>
  </div>
</div>
</form>
</div>


<div class="modal fade" id="jabatanFormModal" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
<form method="post" id="jabatanForm" action="/divisi-jabatan/simpan">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Ubah Jabatan</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body m-3">
        <div class="row">
            <div class="col-md-2">No</div>
            <div class="col-md-8">Nama</div>
        </div>
        @csrf
        <input name="change_key" value="title" class="form-control" hidden/>
        @foreach($title as $key=>$t) 
        <div class="row mt-2">
            <div class="col-md-2">{{$key+1}}<input class="form-control" name="ids[]" value="{{$t->id}}" hidden/></div>
            <div class="col-md-8"><input class="form-control" name="titles[]" value="{{$t->name}}"/></div>
        </div>
        @endforeach
        <div id="new-row-title"></div>
        
      <div class="row justify-content-md-center">
        <button type="button" id="add-title" class="btn btn-outline-primary col-11 mt-4">Tambah Jabatan</button>    
      </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="save-btn">Simpan</button>
    </div>
  </div>
</div>
</form>
</div>
@endsection

@section('adminContent')
<div class="row">
    <div class="col-12 col-lg-6 d-flex">
        <div class="card flex-fill">
            <div class="card-header mb-3">
                <div class="float-end">
                        <a href="#" style="color:#ffffff; font-size:12px;"class="btn btn-primary mt-n1" id="btn-change-division" data-bs-toggle="modal" data-last="{{ count($division) }}" data-bs-target="#divisionFormModal">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                            </svg>
                            Ubah Divisi
                        </a>
                </div>
                <h5 class="card-title mb-0"><strong>Master Data</strong> Divisi</h5>
            </div>
            <div id="datatables-reponsive_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <table id="datatables-reponsive" class="table table-striped dataTable no-footer dtr-inline collapsed" style="width: 100%;" role="grid" aria-describedby="datatables-reponsive_info">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th class="d-none d-xl-table-cell">Nama Divisi</th>
                        </tr>
                    </thead>
                    <tbody >
                    @foreach($division as $key=>$d)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td class="d-none d-xl-table-cell">{{$d->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-6 d-flex">
        <div class="card flex-fill">
            <div class="card-header mb-3">
                <div class="card-actions float-end">
                        <a href="#" style="color:#ffffff; font-size:12px;"class="btn btn-primary mt-n1" id="btn-change-title" data-bs-toggle="modal" data-last="{{ count($title) }}" data-bs-target="#jabatanFormModal">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                            </svg>
                            Ubah Jabatan
                        </a>
                </div>
                <h5 class="card-title mb-0"><strong>Master Data</strong> Jabatan</h5>
            </div>
            <div id="datatables-reponsive_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <table id="datatables-reponsive" class="table table-striped dataTable no-footer dtr-inline collapsed" style="width: 100%;" role="grid" aria-describedby="datatables-reponsive_info">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th class="d-none d-xl-table-cell">Nama Jabatan</th>
                        </tr>
                    </thead>
                    <tbody >
                    
                    @foreach($title as $key=>$t)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td class="d-none d-xl-table-cell">{{$t->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function() {

    var lastDivNo = $('#btn-change-division').data('last');   
    var lastTittleNo = $('#btn-change-title').data('last');

    $('#divisionFormModal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#save-btn').on('click',function() {
            document.getElementById('divisionForm').submit();
        });

        var btnAdd = $('#add-division');
        btnAdd.prop("disabled", lastDivNo >=20);
    });

    $('#titleFromModal').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('#save-btn').on('click', function() {
            document.getElementById('titleForm').submit();
        });

        var btnAdd = $('#add-title');
        btnAdd.prop("disabled", lastTittleNo >= 20);
    });

    $('#add-division').on('click', function() {
        lastDivNo = lastDivNo +1;
        var newNode = createNewInputNode(lastDivNo, "divisions");
        document.getElementById('new-row').appendChild(newNode);
      
        var btnAdd = $('#add-division');
        btnAdd.prop("disabled", lastDivNo >=20);
    });

    $('#add-title').on('click', function() {
        lastTittleNo = lastTittleNo +1;
        var newNode = createNewInputNode(lastTittleNo, "titles");
        document.getElementById('new-row-title').appendChild(newNode);
       
        var btnAdd = $('#add-title');
        btnAdd.prop("disabled", lastTittleNo >= 20);
    });
});

function createNewInputNode(newNum, key) {
    var rowNode = document.createElement('DIV');
    rowNode.setAttribute("class","row mt-3");
    var id = "i-"+key+"-"+newNum;
    rowNode.setAttribute("id", id);
    
    var numNode = document.createElement('DIV');
    numNode.setAttribute("class", "col-md-2");
   
    var inputNode = document.createElement('DIV');
    inputNode.setAttribute("class", "col-md-8");
    var input = document.createElement('INPUT');
    input.setAttribute("class", "form-control");
    input.setAttribute("name", key+"[]");
    inputNode.appendChild(input);

    var delNode = document.createElement('A');
    delNode.setAttribute("id", "del-input");
    delNode.setAttribute("class", "col-md-1");
    delNode.setAttribute("style", "color:red;");
    delNode.setAttribute("onclick", "deleteInput('"+id+"')");
    var icon = document.createElement('i');
    icon.setAttribute("class", "fa fa-close mt-2 p-0");
    delNode.appendChild(icon);

    rowNode.appendChild(numNode);
    rowNode.appendChild(inputNode);
    rowNode.appendChild(delNode);

    return rowNode;
}

function deleteInput(id) {
    var inputEl = document.getElementById(id);
    inputEl.remove();
}

</script>
@endsection

