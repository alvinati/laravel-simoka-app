@extends('admin/main')

@section('adminContent')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div id="datatables-reponsive_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
          <div class="row">
            <div class="col-sm-12">
              <table id="datatables-reponsive" class="table table-striped dataTable no-footer dtr-inline collapsed" style="width: 100%;" role="grid" aria-describedby="datatables-reponsive_info">
                <thead>
                  <tr>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Nama: activate to sort column ascending">
                      Nama Pegawai
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Keperluan: activate to sort column ascending">
                      Keperluan
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Tujuan: activate to sort column ascending">
                      Tujuan
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Pengemudi: activate to sort column ascending">
                      Pengemudi
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Tanggal Berangkat: activate to sort column ascending">
                      Tanggal Berangkat
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Jam Berangkat: activate to sort column ascending">
                      Jam Berangkat
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Tanggal Kembali: activate to sort column ascending">
                      Tanggal Pulang
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="datatables-reponsive" rowspan="1" colspan="1" style="" aria-label="Aksi: activate to sort column ascending">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>	
                  @foreach($bookings as $b)
                    @if($loop->iteration % 2 == 0)									
                    <tr class="odd">
                        <td tabindex="0">{{ $b->requestor }}</td>
                        <td>{{$b->request->requisite}}</td>
                        <td>{{$b->request->destination}}</td>
                        <td>{{$b->driver}}</td>
                        <td style="">{{$b->exit_date}}</td>
                        <td style="">{{$b->request->travel_time}}</td>
                        <td style="">{{$b->return_date }}</td>
                        <td class="table-action">
                            <a class="detail-icon" href="/laporan/{{$b->id}}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            </a>
                        </td>
                    </tr>

                    @else
                    <tr class="even">
                        <td tabindex="0">{{ $b->requestor }}</td>
                        <td>{{$b->request->requisite}}</td>
                        <td>{{$b->request->destination}}</td>
                        <td>{{$b->driver}}</td>
                        <td style="">{{$b->exit_date}}</td>
                        <td style="">{{$b->request->travel_time}}</td>
                        <td style="">{{$b->return_date }}</td>
                        <td class="table-action">
                            <a class="detail-icon" href="/laporan/{{$b->id}}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text align-middle me-2 text-primary"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            </a>
                        </td>
                    </tr>
                        @endif
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-5">
                <div class="dataTables_info" id="datatables-reponsive_info" role="status" aria-live="polite">
                 
                </div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers" id="datatables-reponsive_paginate">
                <ul class="pagination mt-3">
                  {{ $bookings->links() }}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
</script>
@endsection
