@extends('admin/main')

@section('styles')
<style>
    th {
        font-size:14px;
    }
    td {
        font-size:14px;
    }
</style>
@endsection

@section('adminContent')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3 ><strong>
                @if($data->status_id == 1 && $action == 'validasi')
                    Validasi
                @elseif($data->status_id == 1 && $action == 'tolak')
                    Tolak
                @else 
                    Detail
                @endif
                </strong> Pengajuan</h3>
                <h5 class="card-subtitle text-muted mt-1">Detail pengajuan peminjaman kendaraan</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-auto ms-auto text-end mt-n1">
                <h4><strong>Nomor. </strong>{{$data->request_number}}</h4>
            </div>
        </div>
      </div>
      <div class="card-body">
        @if($action == 'validasi')
        <form method="post" action="/pengajuan/save">
        @elseif($action == 'tolak')
        <form method="post" action="/pengajuan/decline">
        @else
        <form method="post">
        @endif
            <table class="table table-md mt-2 mb-5">
                <tbody>
                    <tr class="mb-2">
                        <th>Tanggal Pengajuan</th>
                        <td>: {{$data->created_at->locale('id')->isoFormat('dddd, D MMMM Y') }}</td>
                        <th>Tujuan</th>
                        <td>: {{ $data->destination }}</td>
                    </tr>
                    <tr>
                        <th>Nama Pegawai</th>
                        <td>: {{ $data->employee->name }}</td>
                        <th>Tanggal Berangkat</th>
                        <td>: {{ $data->travel_date }}</td>
                    </tr>
                    <tr>
                        <th>Keperluan</th>
                        <td>: {{ $data->requisite->requisite }}</td>
                        <th>Jam Berangkat</th>
                        <td>: {{ $data->travel_time }}</td>
                    </tr>
                    <tr>
                        <th>Jumlah Penumpang</th>
                        <td>: {{$data->total_passanger}} Orang</td>
                        <th>Status</th>
                        @if($data->status_id == 1)
                        <td>: <span class="badge bg-success">{{ $data->status->status }}</span></td>
                        @elseif($data->status_id == 2)
                        <td>: <span class="badge bg-warning">{{ $data->status->status }}</span></td>
                        @elseif($data->status_id == 3)
                        <td>: <span class="badge bg-danger">{{ $data->status->status }}</span></td>
                        @else
                        <td>: <span class="badge bg-secondary">{{ $data->status->status }}</span></td>
                        @endif
                    </tr>
                    @csrf
                    <input type="hidden" id="id" name="id" value="{{$data->id}}">
                    <tr>
                        @if($data->status_id == 1 && $action=='validasi')
                        <th>Pilih Driver</th>
                        <td><select style="width:180px;" class="form-select" aria-label="Pilih Driver" id="driver" name="driver">
                                <option value="0" selected>Pilih Driver</option>
                                @if($drivers != NULL && count($drivers) > 0)
                                @foreach($drivers as $d)
                                 <option value="{{$d->id}}">{{$d->name}}</option>
                                @endforeach
                                @endif
                            </select>
                        </td>
                        @elseif($data->status_id == 1 && $action=='tolak')
                        <th>Keterangan</th>
                        <td>
                            <textarea class="form-control"  id="note" name="note"></textarea>
                        </td>
                        @elseif($data->status_id == 3)
                        <th>Keterangan Penolakan</th>
                        <td class="text-danger">: {{ $data->notes }}</td>
                        @else
                        <th>Driver</th>
                        <td>: {{$data->driver_name}}</td>
                        @endif
                    </tr>
                </tbody>
            </table>
            @if($data->status_id == 1)
            <div class="row justify-content-md-center">
                @if($action == 'validasi')
                <button type="submit" class="col-md-2 btn btn-primary me-3 btn-save">Simpan </button>
                @elseif($action == 'tolak')
                <button type="submit" class="col-md-2 btn btn-danger me-3 btn-decline">Tolak </button>
                @endif
                <a href="/pengajuan" class="col-md-2 btn btn-secondary">Batal </a>
            </div>
            @endif
        </form>
      </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function() {
 
});

</script>
@endsection
