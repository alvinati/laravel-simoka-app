@extends('templates/master')

@section('meta')
    <meta name="keywords" content="Login, Simoka, Sistem informasi, Mobil Kantor, Peminjaman Mobil, Kendaraan Operasional, Kendaraan Dinas, Dinas">
@endsection

@section('content')

<main class="d-flex w-100">
    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                 <div class="d-table-cell align-middle">
                    <div class="card">
                        <div class="card-body">
                            <div class="m-sm-4 text-center">

                                <h5> Buat Kata Sandi Baru </h5>

                                <form class="text-center" method="POST" action="/reset-password">
                                    @csrf

                                    <label for="inputPassword" class="visually-hidden">Kata Sandi</label>
                                    <input type="password" name="newPassword" id="inputPassword" class="form-control mt-4 mb-4" placeholder="Kata Sandi" required>

                                    <label for="inputRepeatPassword" class="visually-hidden">Ulangi Kata Sandi</label>
                                    <input type="password" name="repeatPassword" id="inputRepeatPassword" class="form-control mt-4 mb-4" placeholder="Ulangi Kata Sandi" required>

                                    <button class="w-50 btn btn-md btn-primary" type="submit">Simpan</button>
                                </form>

                            </div>
                        </div>        
                    </div>
               </div> 
            </div>
        </div>
    </div>
</main>
@endsection
