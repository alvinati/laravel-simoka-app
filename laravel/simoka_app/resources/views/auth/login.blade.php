@extends('templates/master')

@section('meta')
    <meta name="keywords" content="Login, Simoka, Sistem informasi, Mobil Kantor, Peminjaman Mobil, Kendaraan Operasional, Kendaraan Dinas, Dinas">
@endsection


@section('content')
<main class="d-flex w-100">
    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                 <div class="d-table-cell align-middle">
                 @if($errors->any())
                        <div class="alert alert-danger mt-3">
                            <div class="row" style="width:100%;">
                                <div class="col-md-11">
                                    <h5 class="font-weight-bold text-danger" style="margin:10px">Login gagal! </h5>
                                    <p>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul></p>
                                </div>
                                <div class="col-md-1" style="text-align:right; padding-right:0px; padding-left=0px; padding-top:10px;">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-body">
                            <div class="m-sm-4">
                                <form class="text-center" method="POST" action="/login">
                                    
                                    @csrf
                                    
                                    <label for="inputEmail" class="visually-hidden">Alamat Email</label>
                                    <input type="text" id="inputEmail" class="form-control" name='email' placeholder="Email" required autofocus>

                                    <label for="inputPassword" class="visually-hidden">Kata Sandi</label>
                                    <input type="password" id="inputPassword" class="form-control mt-4 mb-4" name="password" placeholder="Kata Sandi" required>

                                    <div class="checkbox mb-3">
                                        <label>
                                            <input type="checkbox" name="remember">
                                            Ingat Login Saya
                                        </label>
                                    </div>

                                    <button class="w-50 btn btn-md btn-primary" type="submit">Login</button>
                                </form>
                                
                                <p class="text-center fs-6 fw-lighter"><a href="/forgot-password">Lupa Kata Sandi?</a></p>
                                <p class="mt-5 mb-3 text-muted text-center">{{ config('app.name') }} © 2020-2021</p>
                            </div>
                        </div>        
                    </div>
               </div> 
            </div>
        </div>
    </div>
</main>
@endsection
