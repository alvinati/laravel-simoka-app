@extends('templates/master')

@section('meta')
    <meta name="keywords" content="Login, Simoka, Sistem informasi, Mobil Kantor, Peminjaman Mobil, Kendaraan Operasional, Kendaraan Dinas, Dinas">
@endsection

@section('content')

<main class="d-flex w-100">
    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                 <div class="d-table-cell align-middle">
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="m-sm-4">

                                <h3>Masukkan NIK / Email Anda</h3>
                                <p class="fs-6">Tautan untuk mengatur ulang kata sandi akan dikirimkan melalui email</p>

                                <form  class="text-center" method="POST" action="/forgot-password">
                                    @csrf
                                    <label for="inputEmail" class="visually-hidden">Alamat Email</label>
                                    <input type="text" name="userEmail" id="inputEmail" class="form-control" placeholder="Email" required autofocus>

                                    <button class="w-50 btn btn-md btn-primary mt-4" type="submit">Kirim</button>
                                </form>
                                
                                <p class="mt-5 mb-3 text-muted">{{ config('app.name') }} © 2020-2021</p>

                            </div>
                        </div>        
                    </div>
               </div> 
            </div>
        </div>
    </div>
</main>
@endsection
