<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\AccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', IndexController::class);


//Authentication Region
Route::get('login', function() {
    return view('auth.login');
});
Route::post('login', [AuthController::class, 'doLogin'])->name('login');
Route::get('logout', [AuthController::class, 'logout']);
Route::get('reset-password', function() {
    return view('auth.reset-password');
});
Route::post('/reset-password', [AuthController::class, 'newPassword'])->middleware('guest')->name('password.reset');
Route::get('forgot-password', function() {
    return view('auth.forgot-password');
});
Route::post('/forgot-password', [AuthController::class, 'reqResetPassword']);


//Administration region
Route::middleware(['auth'])->group(function(){
    
    Route::get('/admin', AdminController::class)->name('admin');
    Route::get('/pengajuan', [AdminController::class, 'requestLists']);
    Route::get('/pengajuan/{id}/{action}',[AdminController::class, 'validation']);
    Route::post('/pengajuan/save',[AdminController::class, 'acceptRequest'])->name('pengajuan.save');
    Route::post('/pengajuan/decline', [AdminController::class, 'declineRequest'])->name('pengajuan.decline');
    Route::get('/peminjaman', [AdminController::class, 'bookings']);
    Route::get('/peminjaman/{id}', [AdminController::class, 'detailBooking']);
    Route::post('/peminjaman/{id}/finish', [AdminController::class, 'finishBooking']);

    Route::get('/laporan', ReportController::class);
    Route::get('/laporan/{id}', [ReportController::class, 'reportDetail']);
        
    Route::get('/pegawai', EmployeeController::class);
    Route::get('/pegawai/delete/{id}', [EmployeeController::class, 'delete'])->name('pegawai.delete');
    Route::get('/pegawai/search', [EmployeeController::class, 'search'])->name('pegawai.search');
    Route::post('/pegawai/save', [EmployeeController::class, 'save'])->name('pegawai.save');
    Route::post('/pegawai/update', [EmployeeController::class, 'update'])->name('pegawai.update');

    Route::get('/kendaraan', VehicleController::class)->name('kendaraan');
    Route::get('/kendaraan/delete/{id}', [VehicleController::class, 'delete'])->name('kendaraan.delete');
    Route::get('/kendaraan/search', [VehicleController::class, 'search'])->name('kendaraan.search');
    Route::post('/kendaraan/save', [VehicleController::class, 'save'])->name('kendaraan.save');
    Route::post('/kendaraan/update', [VehicleController::class, 'update'])->name('kendaraan.update');

    Route::get('/pengemudi', DriverController::class);
    Route::get('/pengemudi/search', [DriverController::class, 'search'])->name('pengemudi.search');
    Route::post('/pengemudi/update', [DriverController::class, 'update'])->name('pengemudi.update');

    Route::get('/divisi-jabatan', OrganizationController::class);
    Route::post('/divisi-jabatan/simpan', [OrganizationController::class, 'saveChanges']);

    Route::get('/profil', AccountController::class);
});
