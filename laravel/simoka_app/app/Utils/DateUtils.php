<?php

namespace App\Utils;

class DateUtils {

    public static function toStringDateLocaleID($date) {
        return $date->locale('id')->isoFormat('dddd, D MMMM Y');
    }
}