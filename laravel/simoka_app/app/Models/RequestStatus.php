<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Model\UserRequest;

class RequestStatus extends Model {

    protected $table = 'request_status';

    public function request() {
        return $this->hasMany(UserRequest::class);
    }

}