<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Model\Employee;

class EmployeeDivision extends Model {

    
    protected $table =  'employee_division';

    public function employee() {
        return $this->hasMany(Employee::class);
    }
}