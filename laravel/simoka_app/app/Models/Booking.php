<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\UserRequest;
use App\Models\Employee;
use App\Models\BookingStatus;
use App\Models\BookingReceipt;

class Booking extends Model { 
  
    protected $table = 'bookings';

    public function request() {
        return $this->belongsTo(UserRequest::class);
    }

    public function employee() {
        return $this->belongsTo(Employee::class);
    }

    public function status() {
        return $this->belongsTo(BookingStatus::class);
    }

    public function receipts() {
        return $this->hasMany(BookingReceipts::class);
    }

}