<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Model\Employee;

class EmployeeTitle extends Model {

    
    protected $table =  'employee_title';

    public function employee() {
        return $this->hasMany(Employee::class);
    }
}