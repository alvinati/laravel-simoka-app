<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Employee;
use App\Models\Driver;
use App\Models\Requisite;
use App\Models\RequestStatus;
use App\Models\Booking;

class UserRequest extends Model
{
    use HasFactory;

    protected $table = 'requests';

    public function employee() {
        return $this->belongsTo(Employee::class);
    }

    public function driver() {
        return $this->belongsTo(Driver::class);
    }

    public function requisite() {
        return $this->belongsTo(Requisite::class);
    }

    public function status() {
        return $this->belongsTo(RequestStatus::class);
    }

    public function usage() {
        return $this->belongsTo(Booking::class);
    }
}


