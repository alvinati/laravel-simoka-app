<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Driver;
use App\Models\VehicleStatus;

class Vehicle extends Model {

    protected $table = 'cars';

    public function status() {
        return $this->belongsTo(VehicleStatus::class);
    }

    public function driver() {
        return $this->hasOne(Driver::class);
    }
}