<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Vehicle;

class VehicleStatus extends Model {

    protected $table = 'car_status';

    public function car() {
        return $this->hasMany(Vehicle::class);
    }

}