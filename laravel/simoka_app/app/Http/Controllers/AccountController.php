<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Employee;
use App\Models\EmployeeDivision;
use App\Models\EmployeeTitle;

class AccountController extends Controller
{
    public function __invoke() {
        $userName = Auth::user()->name;
        $userId = Auth::user()->id;
        $notifications = ['sancai', 'ezra'];

        $employee = Employee::select('name', 'mphone_number', 'email')->where('user_id', $userId)->first();

        return view('admin.profile', ['user_fullname' => $userName, 'notifications' => $notifications,
                    'profile' => $employee]);
    }
}