<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Database\QueryException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\EmployeeTitle;
use App\Models\EmployeeDivision;
use App\Models\UserRole;
use App\Models\User;
use App\Models\Driver;
use App\Models\Vehicle;
use App\Models\UserRequest;


class EmployeeController extends Controller
{
    public function __invoke()
    {

        $user_name = Auth::user()->name;
        $notification = ["Aghi", "Aghia"];

        $employees = Employee::with(['division', 'title', 'user'])->orderBy('id', 'asc')->simplePaginate(5);
        $divisions = EmployeeDivision::all();
        $titles = EmployeeTitle::all();
        $roles = UserRole::all();

        return view('admin.employee', ['user_fullname' => $user_name, 'notifications' => $notification, 
                        'employees' => $employees, 'divisions' => $divisions, 'titles' => $titles, 
                        'roles' => $roles,'searchKey' => '']);
    }

    public function save(EmployeeRequest $request) {

        $currentUserId = Auth::user()->id;

        $validated = $request->validated();

        $additionalValidator = Validator::make($request->all(), [
            'nik' => 'unique:employees',
            'email' => 'unique:employees|unique:users',
            'role' => 'required'
        ], $messages = [
            'nik.unique' => 'Karyawan dengan NIK yang diberikan sudah terdaftar',
            'email.unique' => 'Karyawan / User dengan email yang diberikan sudah terdaftar',
            'role.required' => 'Role user karyawan harus dipilih'
        ]);

        if ($additionalValidator->fails()) {
            return redirect('pegawai')
                        ->withErrors($additionalValidator)
                        ->withInput();
        }

        $defaultPassword = "Simoka21";

        $result =  User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => Hash::make($defaultPassword),
            'role' => $request->role,
            'created_by' => $currentUserId,
            'updated_by' => $currentUserId
        ]);

        $createdUserId = User::where('email',$validated['email'])->first()->id;
      
        // $result = Password::sendResetLink(
        //     $request->only('email')
        // );

        // var_dump($result); return;
    

        $employee = new Employee;
        $employee->NIK = $validated["nik"];
        $employee->user_id = $createdUserId;
        $employee->title_id = $validated["title"];
        $employee->division_id = $validated["division"];
        $employee->name = $validated["name"];
        $employee->email = $validated["email"];
        $employee->mphone_number =  $validated["phoneNumber"];
        $employee->wa_number =  $validated["waNumber"];
        $employee->address = $validated["address"];
        $employee->created_by = $currentUserId;
        $employee->updated_by = $currentUserId;

        $employee->save();

        if($validated['title'] == EmployeeTitle::where('name', 'Driver')->first()->id) {
            $driver = new Driver;
            $driver->employee_id = $employee->id;
            $driver->status_id = 1;
            $driver->updated_by = $currentUserId;
            $driver->created_by = $currentUserId;

            $driver->save();
            
            return redirect('/pegawai')->with('status', 'Data Pegawai dan Pengemudi berhasil ditambahkan!');
        }

        return redirect('/pegawai')->with('status', 'Pegawai berhasil ditambahkan!');
    }


    public function update(EmployeeRequest $request) {

        $currentUserId = Auth::user()->id;
        $validated = $request->validated();
        $employee = Employee::find($request->id);
        $redirect = redirect("/pegawai")->with('status', 'Perubahan berhasil!');

       if($request->email != $employee->email) {
            $additionalValidator = Validator::make($request->all(), [
                'email' => 'unique:employees|unique:users'
            ], $messages = [
                'email.unique' => 'Karyawan / User dengan email yang diberikan sudah terdaftar'
            ]);
        
            if ($additionalValidator->fails()) {
                return redirect('pegawai')
                        ->withErrors($additionalValidator)
                        ->withInput();
            }
       }

       if($request->nik != $employee->NIK) {
            $additionalValidator = Validator::make($request->all(), [
                'nik' => 'unique:employees'
            ], $messages = [
                'nik.unique' => 'Karyawan dengan NIK yang diberikan sudah terdaftar'
            ]);
        
            if ($additionalValidator->fails()) {
                return redirect('pegawai')
                            ->withErrors($additionalValidator)
                            ->withInput();
            }
        }
        
       if($validated['title'] == EmployeeTitle::where('name', 'Driver')->first()->id //if current request title is driver
            && $validated['title'] != $employee->title_id // and if current request title change
            && Driver::where('employee_id', $employee->id)->first() == NULL) { // and if employee id is not exist as drivers table before
            
            $driver = new Driver;
            $driver->employee_id = $employee->id;
            $driver->status_id = 1;
            $driver->created_by = $currentUserId;
            $driver->updated_by = $currentUserId;

            $driver->save();
            
            $redirect =  redirect('/pegawai')->with('status', 'Data Pegawai berhasil diubah dan Data Pengemudi berhasil ditambahkan!');

        } 

       $employee->title_id = $validated["title"];
       $employee->NIK = $validated["nik"];
       $employee->email = $validated["email"];
       $employee->division_id = $validated["division"];
       $employee->name = $validated["name"];
       $employee->mphone_number = $validated["phoneNumber"];
       $employee->wa_number = $validated["waNumber"];
       $employee->address = $validated["address"];
       $employee->updated_by = $currentUserId;

       $employee->save();

       $user = User::where('id', $employee->user_id)->first();
       $user->email = $validated["email"];
       $user->name = $validated["name"];
       $user->role = $validated["role"];
       $user->updated_by = $currentUserId;

       $user->save();

        return $redirect;
    }

    
    public function delete($id) {

        $employee = Employee::find($id);
        $user = User::find($employee->user_id);
        $currentUserId = Auth::user()->id;
        $ifDriver = Driver::where('employee_id', $id)->first();

        if(UserRequest::where('requestor', $id)->first() != NULL) {
            $this->softDeleteEmployee($employee, $user, $currentUserId);
            return redirect('/pegawai')->withErrors(['Pegawai berhasil di non-aktifkan']);

        } else if($ifDriver != NULL){
            $this->softDeleteEmployee($employee, $user, $currentUserId);
            $ifDriver->soft_deleted = true;
            $ifDriver->updated_by = $currentUserId;
            
            if($ifDriver->car_id != NULL) {
                $carAssigned = Vehicle::where('id', $ifDriver->car_id)->first();
                $carAssigned->driver_id = NULL;
                $carAssigned->updated_by = $currentUserId;
                $carAssigned->save();

                $ifDriver->car_id = NULL;
            }

            $ifDriver->save();

            return redirect('/pegawai')->withErrors(['Pegawai berhasil di non-aktifkan']);
        }
        else {
            $employee->delete();
            $user->delete();
            return redirect('/pegawai')->with('status', 'Data pegawai berhasil dihapus!');
        }
    }

    public function search(Request $request) {
       
        if(Employee::all()->count() == 0)
            return redirect('/pegawai')->withErrors(['Data pegawai masih kosong!']);

       $search_query = $request->searchkey;
        
        $user_name = Auth::user()->id;
        $notification = ["Aghi", "Aghia"];

        $employees = Employee::with(['division', 'title'])
                        ->orderBy('id', 'asc')
                        ->where('name','like','%'.$search_query.'%')
                        ->orWhere('nik', 'like', '%'.$search_query.'%')
                        ->orWhere('email', 'like', '%'.$search_query.'%')
                        ->orWhere('wa_number', 'like', '%'.$search_query.'%')
                        ->orWhere('mphone_number', 'like', '%'.$search_query.'%')
                        ->simplePaginate();
     
        $divisions = EmployeeDivision::all();
        $titles = EmployeeTitle::all();
        $roles = UserRole::all();

        return view('admin.employee', ['user_fullname' => $user_name, 'notifications' => $notification, 
                        'employees' => $employees, 'divisions' => $divisions, 'titles' => $titles, 
                        'roles' => $roles,'searchKey' => $search_query]);
                
    }

    private function softDeleteEmployee($employee, $user, $userUpdateId) {
        $employee->soft_deleted = true;
        $employee->updated_by = $userUpdateId;
        $employee->save();

        $user->soft_deleted = true;
        $user->updated_by = $userUpdateId;
        $user->save();
    }

}
