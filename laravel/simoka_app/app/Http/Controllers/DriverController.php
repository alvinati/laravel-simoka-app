<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

use App\Models\Driver;
use App\Models\UserRequest;
use App\Models\Employee;
use App\Models\Vehicle;

class DriverController extends Controller {

    public function __invoke() {
        $user_name = Auth::user()->name;
        $notification = ["Aghi", "Aghia"];

        $drivers = Driver::with(['car', 'employee', 'status'])->orderBy('id', 'asc')->simplePaginate(5);
        $cars = Vehicle::where('driver_id', NULL)->get();
        return view('admin.driver', ['user_fullname' => $user_name, 'notifications' => $notification, 
                        'drivers' => $drivers, 'cars'=>$cars, 'searchKey' => '']);

    }

    public function update(Request $request) {

        $validator = Validator::make($request->all(), [
            'waNumber' => 'required',
            'address' => 'required|max:255',
            'name' => 'required'
        ], $messages = [
            'waNumber.required' => 'Nomor whatsapp tidak boleh kosong',
            'address.required' => 'Alamat tidak boleh kosong',
            'address.max' => 'Alamat tidak boleh melebihi 255 karakter',
            'name.required' => 'Nama tidak boleh kosong'
        ]);

        if ($validator->fails()) {
            return redirect('pengemudi')
                ->withErrors($validator)
                ->withInput();
        }

        $savedDriver = Driver::find($request->id);

        if($request->car_id != NULL && $request->car_id != $savedDriver->car_id) {
            $additionalValidator = Validator::make($request->all(), [
                'car_id' => 'unique:drivers'
            ], $messages = [
                'car_id.unique' => 'Kendaraan dengan plat yang diberikan sudah di assign ke pengemudi lain'
            ]);

            if ($additionalValidator->fails()) {
                return redirect('pengemudi')
                    ->withErrors($additionalValidator)
                    ->withInput();
            }
        }

        $currentUserId = Auth::user()->id;

        $employee = Employee::find($savedDriver->employee_id);
        $employee->name = $request->name;
        $employee->wa_number = $request->waNumber;
        $employee->address = $request->address;
        $employee->updated_by = $currentUserId;

        $employee->save();

        if($request->car_id != 0 && $request->car_id > 0){

            if($savedDriver->car_id != NULL && $request->car_id == $savedDriver->car_id)
                return redirect('pengemudi')->with('status', 'Data pengemudi berhasil diubah!');

            if($savedDriver->car_id != 0){
                $oldCar = Vehicle::find($savedDriver->car_id);
                $oldCar->driver_id = NULL;
                $oldCar->updated_by = $currentUserId;
                $oldCar->save();
            }

            $car = Vehicle::find($request->car_id);
            $car->driver_id = $savedDriver->id;
            $car->updated_by = $currentUserId;
            $car->save();

            $savedDriver->car_id = $request->car_id;
            $savedDriver->updated_by = $currentUserId;
            $savedDriver->save();
        }

        return redirect('pengemudi')->with('status', 'Data pengemudi berhasil diubah!');
    }


    public function search(Request $request) {

        if(Driver::all()->count() == 0)
        return redirect('/kendaraan')->withErrors(['Data kendaraan masih kosong!']);

        $search_query = $request->searchkey;
            
        $user_name = Auth::user()->id;
        $notification = ["Aghi", "Aghia"];

        $drivers = Driver::with(['employee' => function($query) use ($search_query){
                                    $query->where('name','like','%'.$search_query.'%')
                                    ->orWhere('wa_number','like','%'.$search_query.'%');
                             }, 'car'])
                            ->orderBy('id', 'asc')
                            ->get();

        // $drivers_search_car = Driver::with(['employee', 'car' => function($query) use ($search_query){
        //                             $query->where('plat_number','like','%'.$search_query.'%');
        //                         }])
        //                         ->orderBy('id', 'asc')
        //                         ->get();
        // $drivers = (object) array_merge((array)$drivers_search_employee, (array)$drivers_search_car);

        var_dump($drivers);return;

        $cars = Vehicle::where('driver_id', NULL)->get();
        return view('admin.driver', ['user_fullname' => $user_name, 'notifications' => $notification, 
                                        'drivers' => $drivers, 'cars'=>$cars, 'searchKey' => '']);
    }
}