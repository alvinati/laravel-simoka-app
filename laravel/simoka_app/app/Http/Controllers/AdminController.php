<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Models\UserRequest;
use App\Models\Requisite;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\Booking;
use App\Models\BookingStatus;
use App\Models\BookingReceipt;
use App\Utils\StringUtils;
use App\Utils\NumberUtils;



class AdminController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $user_name = Auth::user()->name;
        $notification = ["sancai", "ezra"];
        $now = Carbon::now();
        $date = $now->locale('id')->isoFormat('dddd, D MMMM Y');
      //  $date = $now->formatLocalized('%A, %d %B %Y');
        $time = $now->locale('id')->isoFormat('hh : mm');
        return view('admin.dashboard', ['user_fullname' => $user_name, 'notifications' => $notification, 'currentDate' => $date, 'time' => $time]);
    }

    public function requestLists() {
        $user_name = Auth::user()->name;
        $notification = ["sancai", "ezra"];

        $requests = UserRequest::with(['employee', 'requisite', 'status'])
                        ->orderBy('id', 'asc')
                        ->simplePaginate(5);

        foreach($requests as $r) {
            
            if($r->status->id == 1 && strtoTime($r->travel_date) < time()) { // if waiting and outdated
                $r->status_id = 5; //5 means outdated
                $r->save();
            }

            $r->travel_date = StringUtils::toLocalDateString($r->travel_date);
            
        }

        return view('admin.request', ['user_fullname' => $user_name, 'notifications' => $notification, 
                                        'requests' => $requests]);
    }

    public function validation($requestId, $action) {
        $user_name = Auth::user()->name;
        $notification = ["sancai", "ezra"];
        $drivers = NULL;

        $requestData = UserRequest::where('id', $requestId)->first();
       

        if($requestData->driver_id == NULL){
            $requestData = UserRequest::with(['employee', 'requisite', 'status'])
                ->where('id', $requestId)->first();
        } else {
            $driverId = $requestData->driver_id;
            $driver = Driver::where('id', $driverId)->first();
            $driverName = Employee::where('id', $driver->employee_id)->first()->name;

            $requestData = UserRequest::with(['employee', 'requisite', 'status'])
                ->where('id', $requestId)->first();
            $requestData->{"driver_name"} = $driverName;
        };
       
        if($action == 'validasi'){
            $drivers = DB::table('drivers')
                ->join('employees', 'drivers.employee_id', '=','employees.id')
                ->where('drivers.status_id', 1)
                ->where('drivers.car_id', '>=', 1)
                ->select('drivers.id', 'employees.name')
                ->get();     
        }

        $requestData->travel_date =  StringUtils::toLocalDateString($requestData->travel_date);
        return view('admin.validation', ['user_fullname' => $user_name, 'notifications' => $notification,
            'data' => $requestData, 'drivers' => $drivers, 'action' => $action
        ]);
    }

    public function acceptRequest(Request $request) {
        $userId = Auth::user()->id;
        $notification = ["sancai", "ezra"];

        $url = "pengajuan/".$request->id."/validasi";
    
        if ((int)$request->driver < 1) {
            return redirect($url)
                        ->withErrors("Driver harus dipilih untuk validasi pengajuan, silahkan pilih driver")
                        ->withInput();
        }

        //update request
        $savedData = UserRequest::where('id', $request->id)->first();
        $savedData->driver_id = $request->driver;
        $savedData->updated_by = $userId;
        $savedData->status_id = 2; // 2 means Diterima
        $savedData->save();

        $this->updateDriverAndCar($savedData->driver_id, 2, 2);

        //create new booking
        $newBooking = new Booking;
        $newBooking->book_number = str_replace("RQ", "BK", $savedData->request_number)."-RQ".$savedData->id;
        $newBooking->request_id = $savedData->id;
        $newBooking->exit_date = $savedData->travel_date; //TODO if possible using QR
        $newBooking->status_id = 1; // TODO if possible using QR
        $newBooking->created_by = $userId;
        $newBooking->updated_by = $userId;
        $newBooking->save();

        return redirect($url)->with('status', 'pengajuan berhasil di validasi');
    }

    public function declineRequest(Request $request) {
        $userId = Auth::user()->id;
        $notification = ["sancai", "ezra"];

        $url = "pengajuan/".$request->id."/tolak";

        if(empty($request->note)) {
            return redirect($url)
                ->withErrors("Harap berikan keterangan alasan penolakan pengajuan");
        }

        $savedData = UserRequest::where('id', $request->id)->first();
        $savedData->notes = $request->note;
        $savedData->updated_by = $userId;
        $savedData->status_id = 3;
        $savedData->save();

        return redirect($url)->withErrors('Pengajuan telah ditolak! alasan penolakan: '.$request->note);
    }

    public function bookings() {
        $user_name = Auth::user()->name;
        $notification = ["sancai", "ezra"];

        $bookings = Booking::with(['request','status'])
                        ->orderBy('exit_date', 'desc')
                        ->simplePaginate(5);
  
        if(count($bookings) > 0) {
            foreach($bookings as $key=>$b) {
                $b->exit_date = StringUtils::toLocalDateString($b->exit_date);
               
                if(isset($b->return_date))
                    $b->return_date = StringUtils::toLocalDateString($b->return_date);
    
                $requisite = Requisite::select('requisite')->where('id', $b->request->requisite_id)->first();
                $employee = Employee::select('name')->where('id', $b->request->employee_id)->first();
                $b->request->{'requisite'} = $requisite->requisite;
                $b->{'requestor'} = $employee->name;
            };
        }
       
        return view('admin.booking', ['user_fullname' => $user_name, 'notifications' => $notification, 
                                        'bookings' => $bookings]);
    }

    public function detailBooking($id) {
        $user_name = Auth::user()->name;
        $notification = ["sancai", "ezra"];

        $data = Booking::with(['request', 'status'])->where('id', $id)->first();
        $driver = Driver::select('employee_id', 'car_id')->where('id', $data->request->driver_id)->first();
        $requisite = Requisite::select('requisite')->where('id', $data->request->requisite_id)->first();
        $car = Vehicle::select('plat_number')->where('id', $driver->car_id)->first();
        $driverName = Employee::select('name')->where('id', $driver->employee_id)->first();
        $requestor = Employee::select('name')->where('id', $data->request->employee_id)->first();
        
        $placeHolder = Storage::url('place_holder.png');

        $data->request->travel_date = StringUtils::toLocalDateString($data->request->travel_date);
    
        $data->{'car'} = $car->plat_number;
        $data->{'requisite'} = $requisite->requisite;
        $data->{'driver_name'} = $driverName->name;
        $data->{'requestor'} = $requestor->name;
        $data->{'place_holder'} = $placeHolder;

        if($data->status->id == 2) {
            $fuel_receipts = BookingReceipt::select('image_path')
                                ->where('booking_id', $data->id)
                                ->where('type', 'FUEL')
                                ->get();

            if(count($fuel_receipts) > 0)
                $data->{'fuel_receipt_paths'} = $fuel_receipts;
            
            $data->{'fuel_cost_string'} = NumberUtils::toStringRupiah($data->fuel_cost);
            
            if($data->toll_cost > 0) {
                $toll_receipts = BookingReceipt::select('image_path')
                                    ->where('booking_id', $data->id)
                                    ->where('type', 'TOLL')
                                    ->get();
                                    
                if(count($toll_receipts) > 0) {
                    $data->{'toll_receipt_paths'} = $toll_receipts;
                }

                $data->{'toll_cost_string'} = NumberUtils::toStringRupiah($data->toll_cost);
            }

            $data->{'total_cost_string'} = NumberUtils::toStringRupiah($data->total_cost);
        }

        return view('admin.finishbook', ['user_fullname' => $user_name, 'notifications' => $notification, 
                        'data' => $data]);
    }

    public function finishbooking(Request $request) {
        $userId = Auth::user()->id;
        $notification = ["sancai", "ezra"];
        $minCost = 2000;
       
        $validator = Validator::make($request->all(), [
            'fuel' => 'required|numeric|min:2000',
            'fuel_receipts.*' => 'image|max:2000',
            'toll_receipts.*' => 'image|max:2000'
        ], $messages = [
            'fuel.required' => 'Biaya bensin harus di isi',
            'fuel.min' => 'Harap isi biaya bensin dengan jumlah yang sesuai',
            'fuel_receipts.*' => ['image'=>'File struk bensin yang di upload harus berupa foto'],
            'toll_receipts.*' => ['image'=>'File struk pembayaran toll yang di upload harus berupa foto'],
            'toll_receipts.*' => ['max'=>'Ukuran foto yang di upload tidak boleh melebihi 2 MB'],
            'fuel_receipts.*' => ['max'=>'Ukuran foto yang di upload tidak boleh melebihi 2 MB']
        ]);

        if ($validator->fails()) {
            return redirect('peminjaman/'.$request->id)
                        ->withErrors($validator)
                        ->withInput();
        }
            
        if(!$request->hasFile('fuel_receipts')) {
            return redirect('peminjaman/'.$request->id)
                    ->withErrors('Harap sertakan struk pembayaran bensin');
        }
         
        if($request->toll > 0 && !$request->hasFile('toll_receipts')) {
            return redirect('peminjaman/'.$request->id)
                    ->withErrors('Harap sertakan juga struk pembayaran toll');
        }

        if($request->hasFile('toll_receipts') && $request->toll < $minCost) {
            return redirect('peminjaman/'.$request->id)
                    ->withErrors('Harap isi biaya toll dengan jumlah yang sesuai');
        }

        //save booking info
        $booking = Booking::where('id', $request->id)->first();
        $booking->return_date = Carbon::now();
        $booking->fuel_cost = $request->fuel;
        $booking->toll_cost = $request->toll;
        $booking->updated_by = $userId;
        $booking->total_cost = $request->total;
        $booking->status_id = 2; //where 2 means finished
        $booking->save();

        //save fuel receipt image path to db
        foreach(array_slice($request->fuel_receipts, 0, 3) as $f ) {
            $path = $f->store('fuels');
            $fuel_receipt = new BookingReceipt;
            $fuel_receipt->booking_id = $booking->id;
            $fuel_receipt->image_path = $path;
            $fuel_receipt->created_by = $userId;
            $fuel_receipt->updated_by = $userId;
            $fuel_receipt->type = "FUEL";
            $fuel_receipt->save();
        }

        //save toll receipt image path if any to db
        if($request->hasFile('toll_receipts')) {
            foreach(array_slice($request->toll_receipts, 0, 3) as $t ) {
                $path = $t->store('toll');
                $toll_receipt = new BookingReceipt;
                $toll_receipt->booking_id = $booking->id;
                $toll_receipt->image_path = $path;
                $toll_receipt->created_by = $userId;
                $toll_receipt->updated_by = $userId;
                $toll_receipt->type = "TOLL";
                $toll_receipt->save();
            }
        }

        $request = UserRequest::where('id', $booking->request_id)->first();
        $request->status_id = 4;
        $request->updated_by = $userId;
        $request->save();

        $this->updateDriverAndCar($request->driver_id, 1, 1);

        return redirect('peminjaman/'.$booking->id)->with('status', "Peminjaman".$booking->booking_number." berhasil diselesaikan");
    }

    private function updateDriverAndCar($driverId, $driverNewStatus, $carNewStatus) {
           $userId = Auth::user()->id;

           //update driver
           $driver = Driver::where('id', $driverId)->first();
           $driver->status_id = $driverNewStatus; // 2 means bertugas
           $driver->updated_by = $userId;
           $driver->save();
   
           //update car
           $car = Vehicle::where('id', $driver->car_id)->first();
           $car->status_id = $carNewStatus; //2 means IN USE
           $car->updated_by = $userId;
           $car->save();
    }
}
