<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class AuthController extends Controller {

    public function doLogin(Request $request) {
        
        $email = $request->email;
        $password = $request->password;
        $remember = $request->remember;

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {

            $userRole = User::where('email', $email)
                            ->first()->role;

                
            if($userRole == 1) { //role id in user_role table where role = admin
                return redirect('/admin');
            } else {
                return back()->withErrors(["Halaman untuk user selain admin masih dikembangkan"]);
            }
          
        }

        return back()->withErrors([
            'email' => 'Email/Password yang diberikan belum sesuai'
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function reqResetPassword(Request $request) {
        $userEmail = $request->input('userEmail');
        return "email: ".$userEmail;
    }

    public function newPassword(Request $request) {
        $newPassword = $request->input('newPassword');
        $repeatPassword = $request->input('repeatPassword');
        return "newpass: ".$newPassword." repeat pass: ".$repeatPassword;
    }

}