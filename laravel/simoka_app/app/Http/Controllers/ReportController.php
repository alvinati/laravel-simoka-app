<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Models\UserRequest;
use App\Models\Requisite;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\Booking;
use App\Models\BookingStatus;
use App\Models\BookingReceipt;
use App\Utils\StringUtils;
use App\Utils\NumberUtils;



class ReportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $user_name = Auth::user()->name;
        $notification = ["sancai", "ezra"];

        $bookings = Booking::with(['request', 'status'])
                        ->where('status_id', 2) //2 means finished
                        ->orderBy('return_date')
                        ->simplePaginate(10);

        if(count($bookings) > 0) {
            foreach($bookings as $key=>$b) {
                $b->exit_date = StringUtils::toLocalDateString($b->exit_date);
                $b->return_date = StringUtils::toLocalDateString($b->return_date);
    
                $requisite = Requisite::select('requisite')->where('id', $b->request->requisite_id)->first();
                $employee = Employee::select('name')->where('id', $b->request->employee_id)->first();
                $driverEmployee = Driver::select('employee_id')->where('id', $b->request->driver_id)->first();
                $driver = Employee::select('name')->where('id', $driverEmployee->employee_id)->first();
                $b->request->{'requisite'} = $requisite->requisite;
                $b->{'requestor'} = $employee->name;
                $b->{'driver'} = $driver->name;
            };
        }

        return view('admin.report', ['user_fullname' => $user_name, 'notifications' => $notification, 
                                        'bookings' => $bookings]);

    }

    public function reportDetail($id) {
        $user_name = Auth::user()->name;
        $notification = ["sancai", "ezra"];

        $b = Booking::with(['request'])->where('id', $id)->first();

        $b->exit_date = StringUtils::toLocalDateString($b->exit_date);
        $b->return_date = StringUtils::toLocalDateString($b->return_date);
        $b->fuel_cost = NumberUtils::toStringRupiah($b->fuel_cost);
        $b->toll_cost = NumberUtils::toStringRupiah($b->toll_cost);
        $b->total_cost = NumberUtils::toStringRupiah($b->total_cost);

        $requisite = Requisite::select('requisite')->where('id', $b->request->requisite_id)->first();
        $employee = Employee::with(['division','title'])
                                    ->where('id', $b->request->employee_id)
                                    ->first();
        $b->{'employee'} = $employee;
        $driver = Driver::with(['car', 'employee'])->where('id', $b->request->driver_id)->first();
        $b->request->{'requisite'} = $requisite->requisite;
        $b->{'requestor'} = $employee->name;
        $b->{'driver'} = $driver->employee->name;
        $b->{'car'} = $driver->car->plat_number;

        return view('admin.reportdetail', ['user_fullname' => $user_name, 'notifications' => $notification, 
                                        'data' => $b]);
    }
}