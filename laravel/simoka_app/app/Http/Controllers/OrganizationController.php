<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\EmployeeDivision;
use App\Models\EmployeeTitle;

class OrganizationController extends Controller
{
    public function __invoke() {
        $userName = Auth::user()->name;
        $notification = ["sancai", "ezra"];

        $division = EmployeeDivision::orderBy('id', 'asc')->simplePaginate(10);
        $title = EmployeeTitle::orderBy('id', 'asc')->simplePaginate(10);

        return view('admin.organization', ['user_fullname' => $userName, 'notifications' => $notification,
            'division' => $division, 'title' => $title
        ]);
    }

    public function saveChanges(Request $request) {
        $wording = $request->change_key == "division" ? "divisi" : "jabatan";

        if($request->change_key == "division") {
            $this->changeDivisionData($request);
        } else {
            $this->changeTitleData($request);
        }
        return redirect('/divisi-jabatan')->with('status', 'Perubahan data '.$wording.' berhasil');
    }

    private function changeTitleData(Request $request) {
        $userId = Auth::user()->id;
        $title = NULL;

        $validator = Validator::make($request->all(), [
            'titles.*' => 'required|max:255|string'
        ], $messages = [
            'divisions.*.required' => 'Nama Jabatan tidak boleh kosong'
        ]);

        if($validator->fails()) {
            return redirect('/divisi-jabatan')->withErrors($validator);
        }

        foreach($request->titles as $key=>$t) {
            if($key+1 <= count($request->ids)) {
                $title = EmployeeTitle::where('id', $request->ids[$key])->first();
                $title->name = $t;
                $title->updated_by = $userId;
                $title->save();
            } else {
                $title = new EmployeeTitle;
                $title->name = $t;
                $title->created_by = $userId;
                $title->updated_by = $userId;
                $title->save();
            }
        }
    }

    private function changeDivisionData(Request $request) {
        $userId = Auth::user()->id;
        $division = NULL;

        $validator = Validator::make($request->all(), [
            'divisions.*' => 'required|max:255|string',
        ], $messages = [
            'divisions.*.required'=> 'Nama divisi tidak boleh kosong'
        ]);

        if($validator->fails()) {
            return redirect('/divisi-jabatan')->withErrors($validator);
        }

        foreach($request->divisions as $key=>$d) {
            if($key+1 <= count($request->ids) ){
                $division = EmployeeDivision::where('id', $request->ids[$key])->first();
                $division->name = $d;
                $division->updated_by = $userId;
                $division->save();
            } else {
                $division = new EmployeeDivision;
                $division->name = $d;
                $division->created_by = $userId;
                $division->updated_by = $userId;
                $division->save();
            }
        } 
    }
}

