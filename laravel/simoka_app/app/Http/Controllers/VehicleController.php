<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

use App\Models\VehicleStatus;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\UserRequest;
use App\Http\Requests\VehicleRequest;

class VehicleController extends Controller {

    public function __invoke()
    {
        $user_name = Auth::user()->name;
        $notification = ["Aghi", "Aghia"];

        $vehicles = Vehicle::with(['status'])->where('soft_deleted',0)->orderBy('id', 'asc')->simplePaginate(5);

        return view('admin.vehicle', ['user_fullname' => $user_name, 'notifications' => $notification, 
                        'vehicles' => $vehicles, 'searchKey' => '']);
    }

    public function save(VehicleRequest $request) {
        
        $validated = $request->validated();
        $userId = Auth::user()->id;

        $vehicle = new Vehicle;
        $vehicle->model_version = $validated['model_version'];
        $vehicle->brand = $validated['brand'];
        $vehicle->plat_number = $validated['plat_number'];
        $vehicle->km = $validated['km'];
        $vehicle->soft_deleted = 0; //not get deleted
        $vehicle->status_id = 1;// available
        $vehicle->updated_by = $userId;
        $vehicle->created_by = $userId;

        $vehicle->save();

        return redirect('/kendaraan')->with('status', 'Kendaraan berhasil ditambahkan!');
    }

    public function update(VehicleRequest $request) {
        $validated = $request->validated();
        $userId = Auth::user()->id;

        $savedVehicle = Vehicle::find($request->id);
        if($savedVehicle->plat_number != $request->plat_number){
           
            $additionalValidator = Validator::make($request->all(), [
                'plat_number' => 'unique:cars'
            ], $messages = [
                'plat_number.unique' => 'Kendaraan dengan plat nomor yang diberikan sudah terdaftar'
            ]);
        
            if ($additionalValidator->fails()) {
                return redirect('kendaraan')
                        ->withErrors($additionalValidator)
                        ->withInput();
            }
        }

        $savedVehicle->model_version = $validated['model_version'];
        $savedVehicle->brand = $validated['brand'];
        $savedVehicle->km = $validated['km'];
        $savedVehicle->plat_number = $validated['plat_number'];
        $savedVehicle->updated_by = $userId;

        $savedVehicle->save();

        return redirect('/kendaraan')->with('status', 'Kendaraan berhasil diubah!');
    }

    public function delete($id) {
        $vehicle = Vehicle::find($id);
        $currentUserId = Auth::user()->id;
      
        if(UserRequest::where('car_id', $id)->first() != NULL) {
           if($vehicle->driver_id != NULL) {
               $this->updateDriver($vehicle, $currentUserId);
           }

           $this->softDeleteVehicle($vehicle, $currentUserId);
            
            return redirect('/kendaraan')->withErrors(['Kendaraan tidak dapat dihapus karena terkait dengan data peminjaman, hanya dapat di non aktifkan']);

        } else if($vehicle->driver_id != NULL) {
            $this->updateDriver($vehicle, $currentUserId);
            $driverEmployeeId = Driver::find($vehicle->driver_id)->employee_id;
            $employee = Employee::find($driverEmployeeId)->name;
            $vehicle->delete();
            return redirect('/kendaraan')->with('status', 'Kendaraan berhasil dihapus! pengemudi '.$employee.' sekarang belum terassign kendaraan');
        }
        else {
            $vehicle->delete();
            return redirect('/kendaraan')->with('status', 'Data kendaraan berhasil dihapus!');
        }

        
    }

    public function search(Request $request) {
        if(Vehicle::all()->count() == 0)
            return redirect('/kendaraan')->withErrors(['Data kendaraan masih kosong!']);

       $search_query = $request->searchkey;
        
        $user_name = Auth::user()->id;
        $notification = ["Aghi", "Aghia"];

        $vehicles = Vehicle::with(['status'])
                        ->orderBy('id', 'asc')
                        ->where('plat_number','like','%'.$search_query.'%')
                        ->orWhere('brand', 'like', '%'.$search_query.'%')
                        ->orWhere('model_version', 'like', '%'.$search_query.'%')
                        ->simplePaginate();

        return view('admin.vehicle', ['user_fullname' => $user_name, 'notifications' => $notification, 
                        'vehicles' => $vehicles, 'searchKey' => $search_query]);
    }

    private function updateDriver($vehicle, $currentUserId) {
        $driver = Driver::find($vehicle->driver_id);
        $driver->car_id = 0;
        $driver->updated_by = $currentUserId;
        $driver->save();
    }

    private function softDeleteVehicle($vehicle, $currentUserId) {   
        $vehicle->soft_deleted = true;
        $vehicle->updated_by = $currentUserId;
        $vehicle->save();
    }
}