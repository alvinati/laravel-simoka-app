<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'nik' => 'required|max:255',
            'role' => 'required|int|min:1',
            'division' => 'required|int|min:1',
            'title' => 'required|int|min:1',
            'phoneNumber' => 'required|numeric|digits_between:10,14|starts_with:08',
            'waNumber' => 'required|numeric|digits_between:10,14|starts_with:08',
            'address' => 'required|string|max:255',
            'email' => 'required|max:255|email:rfc,dns'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Field nama tidak boleh kosong, isi sesuai dengan data karyawan',
            'nik.required' => 'Field NIK tidak boleh kosong, isi sesuai dengan data karyawan',
            'division.required' => 'Field divisi tidak boleh kosong, isi sesuai dengan data karyawan',
            'division.min' => 'Divisi karyawan harus ditentukan, Anda dapat memilih berdasarkan dropdown',
            'title.required' => 'Field jabatan tidak boleh kosong, isi sesuai dengan data karyawan',
            'title.min' => 'Jabatan karyawan harus ditentukan, Anda dapat memilih berdasarkan dropdown',
            'role.required' => 'Field Role tidak boleh kosong, isi sesuai dengan data karyawan',
            'role.min' => 'Role user karyawan harus ditentukan, Anda dapat memilih berdasarkan dropdown',
            'phoneNumber.required' => 'Field nomor HP tidak boleh kosong, isi dengan nomor HP aktif karyawan',
            'waNumber.required' => 'Field nomor Whatsapp tidak boleh kosong, isi dengan nomor WA aktif karyawan',
            'address.required' => 'Field alamat tidak boleh kosong, isi sesuai dengan domisili karyawan saat ini',
            'email.required' => 'Field email tidak boleh kosong, isi dengan email kantor karyawan',
            'email.email' => 'Format email tidak valid, harap cek kembali',
            'phoneNumber.digits_between' => 'Nomor HP minimal 10 karakter dan maksimal 14 karakter',
            'phoneNumber.starts_with' => 'Nomor HP tidak valid, isi dengan awalan 08',
            'phoneNumber.numeric' => 'Isi nomor HP dengan angka',
            'waNumber.numeric'=> 'Isi nomor WA dengan angka',
            'waNumber.digits_between' => 'Nomor WA minimal 10 karakter dan maksimal 14 karakter',
            'waNumber.starts_with' => 'Nomor WA tidak valid, isi dengan awalan 08'
        ];
    }
}
