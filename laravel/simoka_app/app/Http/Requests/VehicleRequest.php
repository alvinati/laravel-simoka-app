<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;


class VehicleRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plat_number' => 'required|string',
            'brand' => 'required|string',
            'model_version' => 'required|string',
            'km' => 'required|int|min:0'
        ];
    }

    public function messages()
    {
        return [
            'plat_number.required' => 'Plat nomor harus di isi',
            'brand.required' => 'Merek kendaraan harus diisi',
            'model_version.required' => 'Nama versi model kendaraan harus diisi',
            'km.required' => 'Jarak yang sudah ditempuh mobil harus di isi, minimal 0 (dalam km)'
        ];
    }
}
